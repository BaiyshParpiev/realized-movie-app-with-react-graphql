const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const movieSchema = new Schema({
  name: String,
  genre: String,
  directorId: String,
  rate: {
    type: Number,
    enum: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
  },
  watched: {
    type: Boolean,
    enum: [true, false],
    default: false,
  }
});

module.exports = mongoose.model('Movie', movieSchema);
